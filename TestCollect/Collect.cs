﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TestCollect
{
    class Collect<T> : IList<T>
    {
        public Collect<T> Next { get; set; }
        private Collect<T> Current { get; set; }
        public T Value { get; set; }

        public int Count { get; private set; }

        public bool IsReadOnly => false;

        public T this[int index]
        {
            get { return GetValue(index); }
            set
            {
                if (!IsReadOnly)
                    SetValue(index, value);
                else
                    throw new Exception("Collection is ReadOnly");
            }
        }

        private T GetValue(int index)
        {
            int index2 = 0;
            foreach (var item in this)
            {
                if (index2 == index)
                {
                    return item;
                }
                else
                    index2++;
            }
            throw new IndexOutOfRangeException();
        }

        private void SetValue(int index, T value)
        {
            Collect<T> curr = this;
            if (index > 0 && index < Count)
                for (int i = 0; i < Count; i++)
                {
                    if (i == index)
                    {
                        curr.Value = value;
                    }
                    else
                    {
                        curr = curr.Next;
                    }
                }
        }

        public Collect()
        {
        }

        public Collect(T value)
        {
            this.Value = value;
        }

        private bool state = false;
        public void Add(T value)
        {
            if (!state)
            {
                state = true;
                Count++;
                Value = value;
            }
            else if (Next == null)
            {
                Count++;
                Next = new Collect<T>(value);
                Current = Next;
            }
            else
            {
                Count++;
                Current.Next = new Collect<T>(value);
                Current = Current.Next;
            }
        }

        public int IndexOf(T value)
        {
            int index = 0;
            Collect<T> cur = this;
            while (true)
            {
                if (cur.Value.Equals(value))
                {
                    return index;
                }
                else
                {
                    if (index == Count - 1)
                        break;
                    cur = cur.Next;
                    index++;
                }
            }
            return -1;
        }

        public void Insert(int index, T item)
        {
            Collect<T> temp;
            Current = this;
            for (int i = 0; i < Count; i++)
            {
                if (index == i + 1)
                {
                    temp = Current.Next;
                    Current.Next = new Collect<T>(item);
                    Count++;
                    Current.Next.Next = temp;
                    break;
                }
                else
                {
                    Current = Current.Next;
                }
            }
        }

        public void RemoveAt(int index)
        {
            Collect<T> temp;
            Current = this;
            for (int i = 0; i < Count; i++)
            {
                if (index == i + 1)
                {
                    temp = Current.Next;
                    Current.Next = temp.Next;
                    Count--;
                    break;
                }
                else
                {
                    Current = Current.Next;
                }
            }
        }

        public void Clear()
        {
            Next = null;
            Current = null;
            Count = 0;
        }

        public bool Contains(T item)
        {
            return IndexOf(item) != -1;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = this[arrayIndex];
                arrayIndex++;
            }
        }
        
        public bool Remove(T item)
        {
            if (Contains(item))
            {
                RemoveAt(IndexOf(item));
                return true;
            }
            else
                return false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        private struct Enumerator : IEnumerator<T>
        {
            public Collect<T> EThis { get; set; }

            public T Current { get; private set; }

            object IEnumerator.Current => Current;

            public Enumerator(Collect<T> This)
            {
                EThis = This;
                Current = default(T);
            }

            public bool MoveNext()
            {
                if (EThis == null)
                {
                    return false;
                }
                Current = EThis.Value;
                EThis = EThis.Next;
                return true;
            }

            public void Reset()
            {
                Current = default(T);
            }

            public void Dispose()
            {
            }
        }
    }
}
