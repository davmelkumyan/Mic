﻿using System;
using System.Collections;

namespace TestCollect
{
    class Program
    {
        static void Main(string[] args)
        {
            Collect<int> collection = new Collect<int>();
            collection.Add(10);
            collection.Add(20);
            collection.Add(30);
            collection.Add(40);
            collection.Add(50);
            collection.Add(60);
            collection.Add(70);

            int[] arr = new int[3];
            collection.CopyTo(arr, 4);

            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }

            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }

            //col[3] = 15;
            for (int i = 0; i < collection.Count; i++)
            {
                Console.WriteLine(collection[i]);
            }
            Console.WriteLine(collection.IndexOf(10));
            collection.Insert(2, 90);
            collection.RemoveAt(2);

            Console.WriteLine(collection.Remove(8000));
            Console.WriteLine(collection.Contains(90));

            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }
    }
    
}
